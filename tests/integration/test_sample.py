import boto3, moto, os
from fastapi.testclient import TestClient
from src.main import app

client = TestClient(app)

sample_table_name: str = 'esd_sample_events' #os.environ.get('ESD_SAMPLES_TABLE')

def create_sample_table():
    with moto.mock_dynamodb():
        client = boto3.client('dynamodb')
        client.create_table(
            AttributeDefinitions=[
                {"AttributeName": "sample_id"},
            ],
            TableName=sample_table_name,
            # For this demo we'll use sample_id as the hashkey.
            # In the real thing we'll need to settle on something different.
            KeySchema=[
                {"AttributeName": "sample_id"}
            ],
            BillingMode="PAY_PER_REQUEST"
        )

def test_create_sample():
    sample_id: str = 'LP_123456789'
    code: str = 'SMPL_01'
    program: str = 'newborns'
    source: str = 'illumina'
    json_map: dict = {f"sample_id":{sample_id}, "code": {code}, "program": program, "source": source}

    response = client.post(
        '/createSampleDeliveryRecord',
        json=json_map
    )
    assert response.status_code == 201

    response = client.get(
        f'/sample/{sample_id}'
    )

    response.status_code == 200
    response.json() == json_map