from fastapi.testclient import TestClient
from src.main import app, resolve_mediator
from src.internal.application.mediator import IMediator


class FakeHappyPathMediator(IMediator):
    def send(self, obj: object) -> bool:
        return True


class FakeErrorMediator(IMediator):
    def send(self, obj: object) -> bool:
        return False


client = TestClient(app)

class TestCreateSample():
    route: str = '/createSampleDeliveryRecord'

    def test_create_program_returns_unprocessable_entity_when_no_request_body(self):
        response = client.post(self.route)
        assert response.status_code == 422

    def test_create_program_returns_created_for_happy_path(self):
        app.dependency_overrides[resolve_mediator] = lambda: FakeHappyPathMediator()
        response = client.post(
            self.route,
            json={"sample_id":"LP_123456789", "code": "SMPL_01", "program": "newborns", "source": "illumina"}
        )
        assert response.status_code == 201

    def test_create_program_returns_bad_request_for_command_error(self):
        app.dependency_overrides[resolve_mediator] = lambda: FakeErrorMediator()
        response = client.post(
            self.route,
            json={"sample_id":"LP_123456789", "code": "SMPL_01", "program": "newborns", "source": "illumina"}
        )
        assert response.status_code == 400
