from src.internal.domain.seedwork import *

class FakeEnumeration(Enumeration):
    def __init__(self, id: int, name: str) -> None:
        super().__init__(id, name)
    
    def __eq__(self, other: object) -> bool:
        return super().__eq__(other)

    def __hash__(self) -> int:
        return super().__hash__()

    @classmethod
    def value_one(cls):
        return  cls(1, "value_one".lower())

    @classmethod
    def value_two(cls):
        return cls(2, "value_two".lower())

    @classmethod
    def list(cls) -> set:
        return {
            cls.value_one(),
            cls.value_two(),
        }

    @classmethod
    def from_id(cls, id: int) -> Enumeration:
        return next(item for item in cls.list() if item._Enumeration__id == id)

    @classmethod
    def from_name(cls, name: str) -> Enumeration:
        return next(item for item in cls.list() if item._Enumeration__name == name)


class AnotherFakeEnumeration(Enumeration):
    def __init__(self, id: int, name: str) -> None:
        super().__init__(id, name)
    
    def __eq__(self, other: object) -> bool:
        return super().__eq__(other)

    def __hash__(self) -> int:
        return super().__hash__()

    @classmethod
    def value_one(cls):
        return  cls(1, "value_one".lower())

    @classmethod
    def value_two(cls):
        return cls(2, "value_two".lower())

    @classmethod
    def list(cls) -> set:
        return {
            cls.value_one(),
            cls.value_two(),
        }

    @classmethod
    def from_id(cls, id: int) -> Enumeration:
        return next(item for item in cls.list() if item.id == id)

    @classmethod
    def from_name(cls, name: str) -> Enumeration:
        return next(item for item in cls.list() if item._Enumeration__name == name)


def test_construction_of_enumeration_type():
    FakeEnumeration(1, 'VALUE_ONE')


def test_enumeration_equality_when_same_id():
    assert FakeEnumeration.value_one().__eq__(FakeEnumeration.value_one())


def test_enumeration_not_equal_when_different_id():
    assert not FakeEnumeration.value_one().__eq__(FakeEnumeration.value_two())


def test_enumeration_not_equal_when_compared_to_not_enumeration_type():
    assert not FakeEnumeration.value_one().__eq__('string')


def test_enumeration_not_equal_when_compared_to_different_enumeration_type():
    assert not FakeEnumeration.value_one().__eq__(AnotherFakeEnumeration.value_one())


def test_enumeration_from_id_returns_correct_enumeration_instance():
    assert FakeEnumeration.from_id(1).__eq__(FakeEnumeration.value_one())


def test_enumeration_from_name_returns_correct_enumeration_instance():
    assert FakeEnumeration.from_name('value_one').__eq__(FakeEnumeration.value_one())
    