import pytest
from dataclasses import dataclass
from src.internal.application.mediator import CommandMediator
from src.internal.application.handlers import CommandHandler

@dataclass
class FakeCommandOne():
    pass


class FakeCommandHandlerOne(CommandHandler):
    pass


def test_get_handler_returns_handler_type_for_given_command():
    commands = {FakeCommandOne.__name__: FakeCommandHandlerOne}
    command_to_find = FakeCommandOne()
    mediator = CommandMediator(commands)
    assert mediator.get_handler(command_to_find) == FakeCommandHandlerOne


def test_get_handler_raises_exception_when_command_not_found():
    commands = {}
    command_to_find = FakeCommandOne()
    mediator = CommandMediator(commands)
    with pytest.raises(Exception):
        mediator.get_handler(command_to_find)
