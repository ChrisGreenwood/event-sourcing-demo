import boto3
import os
from typing import List
from src.internal.domain.seedwork import DomainEvent
from src.internal.domain.sampleaggregate import ISampleRepository


class DynamoDbSampleRepository(ISampleRepository):
    def __init__(self,) -> None:
        self.__client = boto3.client(
            'dynamodb',
            endpoint_url=os.environ.get('ESD_DDB_ENDPOINT_URL')
        )

    def append_event(self, event: DomainEvent) -> None:
        response = self.__client.put_item(
            TableName=os.environ.get('ESD_SAMPLES_TABLE'),
            Item=event
        )

    def append_events(self, events: List[DomainEvent]) -> None:
        for event in events:
            self.append_event(event)

    def get_state(self, id: str, end_date: str = None) -> object:
        return super().get_state(id, end_date)
