from __future__ import annotations
from abc import ABC, abstractmethod, abstractproperty


class Enumeration(ABC):
    def __init__(self, id: int, name: str) -> None:
        self.__id = id
        self.__name = name

    def __str__(self) -> str:
        return self.__name

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Enumeration):
            return False
        
        typeMatches = isinstance(other, type(self))
        valueMatches = self.__id.__eq__(other.__id)

        return typeMatches and valueMatches

    def __hash__(self) -> int:
        return self.__id.__hash__()

    @abstractmethod
    def from_name(name: str) -> Enumeration:
        pass

    @abstractmethod
    def from_id(id: int) -> Enumeration:
        pass

    @abstractmethod
    def list() -> set:
        pass


class Entity(ABC):
    def __init__(self) -> None:
        self.__domain_events = []
        self.__event_counter = 0

    def raise_event(self, event: DomainEvent):
        """
        Logic for incrementing event sequence number is contained in the base class.
        This number needs to be offset by the max sequence number found for an aggregateId
        when persisting to the event store.
        """
        self.__event_counter += 1
        event.sequence_number = self.__event_counter
        self.__domain_events.append(event)



class DomainEvent(ABC):
    @abstractproperty
    def aggregate_id(self) -> str:
        pass

    @abstractproperty
    def sequence_number(self) -> int:
        pass

    @abstractproperty
    def datetimestamp(self) -> str:
        pass

    @abstractproperty
    def data(self) -> object:
        pass
