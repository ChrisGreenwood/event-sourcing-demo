class SampleDeliveredDomainEvent():
    def __init__(self, sample_id: str, code: str, program: str, source: str) -> None:
        self._sample_id = sample_id
        self._code = code
        self._program = program
        self._source = source
