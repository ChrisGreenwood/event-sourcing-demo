from abc import ABC, abstractmethod
from typing import List
from src.internal.domain.seedwork import DomainEvent, Entity, Enumeration
from src.internal.domain.events import SampleDeliveredDomainEvent


class SampleType(Enumeration):
    def __init__(self, id: int, name: str) -> None:
        super().__init__(id, name)
    
    def __eq__(self, other: object) -> bool:
        return super().__eq__(other)

    def __hash__(self) -> int:
        return super().__hash__()

    @classmethod
    def illumina(cls):
        return  cls(1, "illumina".lower())

    @classmethod
    def oxford_nanopore(cls):
        return cls(2, "oxford-nanopore".lower())

    @classmethod
    def list(cls) -> set:
        return {
            cls.illumina(),
            cls.oxford_nanopore(),
        }

    @classmethod
    def from_id(cls, id: int) -> Enumeration:
        return next(item for item in cls.list() if item._Enumeration__id == id)

    @classmethod
    def from_name(cls, name: str) -> Enumeration:
        return next(item for item in cls.list() if item._Enumeration__name == name)


class Sample(Entity):
    def __init__(self, sample_id: str, code: str, program: str, source: str) -> None:
        self.__sample_id = sample_id
        self.__code = code
        self.__program = program
        self.__sample_type = SampleType.from_name(source)
        self.add_sample_delivered_domain_event()
        super().__init__()

    @property
    def code(self) -> str:
        """Returns the internal identifier for the Sample"""
        return self.__code

    @property
    def program(self) -> str:
        """Returns the program the Sample is part of"""
        return self.__program

    @property
    def sample_id(self) -> str:
        """Returns the Sample Id"""
        return self.__sample_id
    
    @property
    def sample_type(self) -> SampleType:
        return self.__sample_type

    @property
    def domain_events(self) -> List[DomainEvent]:
        return self._Entity__domain_events

    def add_sample_delivered_domain_event(self, sample_id: str, code: str, program: str, source: str):
        self.__sample_delivered_domain_event = SampleDeliveredDomainEvent(sample_id, code, program, source)
        self.raise_event(self.__sample_delivered_domain_event)


class ISampleRepository(ABC):
    """
    Interface definiting Sample persistence behaviours
    """
    @abstractmethod
    def append_event(self, event: DomainEvent) -> None:
        """Appends an event to the event store."""
        pass

    @abstractmethod
    def append_events(self, event: List[DomainEvent]) -> None:
        """Appends an event to the event store."""
        pass

    @abstractmethod
    def get_state(self, id: str, end_date: str = None) -> object:
        """
        Gets the state of the given entity id up to the 
        given end_date.
        The latest state is returned if end_date is not 
        specified.
        """
        pass

