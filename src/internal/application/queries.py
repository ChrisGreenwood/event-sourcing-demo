from abc import ABC, abstractmethod
from src.internal.domain.sampleaggregate import ISampleRepository, Sample


class ISampleQueries(ABC):
    @abstractmethod
    def get_sample(sample_id: str):
        pass


class SampleQueries(ISampleQueries):
    def get_sample(sample_id: str, repository: ISampleRepository) -> Sample:
        return repository.get_state(sample_id)