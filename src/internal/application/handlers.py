from abc import ABC
from src.helpers import get_traceback
from src.internal.application.commands import CreateSampleDeliveryRecordCommand, SampleQcPassedCommand
from src.internal.domain.sampleaggregate import Sample, ISampleRepository


class CommandHandler(ABC):
    def __init__(self) -> None:
        pass

    def handle(self, request: object) -> None:
        pass


class CreateSampleDeliveryRecordCommandHandler(CommandHandler):
    def __init__(self, repository: ISampleRepository) -> None:
        self.__repository = repository

    def handle(self, request: CreateSampleDeliveryRecordCommand) -> None:
        try:
            sample = Sample(request.sample_id, request.code, request.program, request.source)
            self.__repository.append_events(sample.domain_events)
        except Exception as e:
            print(get_traceback(e))


class SampleQcPassedCommandHandler(CommandHandler):
    def handle(request: SampleQcPassedCommand) -> None:
        print(f"Inside {SampleQcPassedCommandHandler.__name__}")
