from dataclasses import dataclass


@dataclass
class CreateSampleDeliveryRecordCommand():
    sample_id: str
    code: str
    program: str
    source: str


@dataclass
class SampleQcPassedCommand():
    sample_id: str

