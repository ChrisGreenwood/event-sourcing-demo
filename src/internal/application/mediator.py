import logging
from abc import ABC, abstractmethod
from src.internal.infrastructure.repository import DynamoDbSampleRepository
from src.internal.application.commands import CreateSampleDeliveryRecordCommand, SampleQcPassedCommand
from src.internal.application.handlers import CommandHandler, CreateSampleDeliveryRecordCommandHandler, SampleQcPassedCommandHandler

"""
Dependencies
"""
sample_repository = DynamoDbSampleRepository()

"""
Anytime a new Command/Handler combination is created it must be 
added to this dictionary. This is helpful because it:
- prevents modification of existing CommandMediator, Command or 
  Handler code
- makes explicit where dependency's are managed.
Making use of the Dependency Inversion principle, each 
CommandHandler should only depend on an abstraction.
"""
_COMMANDS = {
    CreateSampleDeliveryRecordCommand.__name__: CreateSampleDeliveryRecordCommandHandler(sample_repository),
    SampleQcPassedCommand.__name__: SampleQcPassedCommandHandler,
}


class IMediator(ABC):
    """
    Interface describing a Meditors behaviours.
    """
    @abstractmethod
    def send(self, obj: object) -> bool:
        pass


class CommandMediator(IMediator):
    """
    Simple mediator implementation to handle dispatch to the correct
    CommandHandler type for a given Command type.
    """
    def __init__(self, commands = _COMMANDS):
        self.__commands = commands

    def get_handler(self, command: object) -> CommandHandler:
        command_name = type(command).__name__
        if command_name not in self.__commands:
            raise Exception(f'[{command_name}] not found.')
        return self.__commands.get(command_name)

    def send(self, command: object) -> bool:
        try:
            handler = self.get_handler(command)
            handler.handle(command)
            return True
        except:
            logging.error(f'Something went wrong when handling [{type(command).__name__}]')
            return False
