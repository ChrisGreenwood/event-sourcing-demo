import logging
from fastapi import Depends, FastAPI, HTTPException
from src.internal.application.mediator import IMediator, CommandMediator
from src.internal.application.commands import CreateSampleDeliveryRecordCommand
from src.internal.application.queries import ISampleQueries, SampleQueries


app = FastAPI()

def resolve_mediator() -> CommandMediator:
    return CommandMediator()


def resolve_queries() -> ISampleQueries:
    return SampleQueries


@app.get("/")
def root():
    return {"message": "Hello world"}


@app.get("/sample/{sample_id}", status_code=200)
def get_sample(sample_id: str):
    try:
        resolve_queries.get_sample(sample_id)
    except:
        raise HTTPException(status_code=400, detail='Something went wrong.')


@app.post("/createSampleDeliveryRecord", status_code=201)
def create_program_rule(
    command: CreateSampleDeliveryRecordCommand,
    mediator: IMediator = Depends(resolve_mediator)
):
    try:
        if not mediator.send(command):
            logging.error(f'Unable to dispatch command [{type(command).__name__}]')
            raise Exception
        return
    except:
        raise HTTPException(status_code=400, detail='Something went wrong.')
